#!/usr/bin/python3

from LearnerLogic import LearnerLogic
from LearnerView import LearnerView
import tkinter

learner_logic = LearnerLogic()

root = tkinter.Tk()
root.title("Dutch Learner")

view = LearnerView(root, learner_logic)
view.start()
root.mainloop()
