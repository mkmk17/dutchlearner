import tkinter
from tkinter import ttk, N, W, E, S, NSEW, EW
from LearnerLogic import LearnerLogic
from typing import Callable, Optional, List
import random
import Utils


class LearnerView:
    """
    GUI of Learner

    :attr root:                   Root for tkinter
    :attr mainframe:              Main frame for other widgets
    :attr logic:                  Reference to class responsible for logic of the program.
    :attr _vars:                  Even if tkinter variable is bind to label by textvariable
        it can still be removed by GC after going out of scope. So I'm keeping them here.
    :attr _vcmd:                  Registered validation function for just digits input
    :attr _shuffled_questions:    List of questions for current quiz session.
    :attr _total_questions_no:    How many questions should be asked this session.
        Should be <= number of _shuffled_questions
    :attr _incorrect_answers:     List of indexes of _shuffled_questions list where user gave incorrect answer.
    :attr _feedback:              Whether user answered last question correctly.
    :attr _feedback_str:          Text description for feedback.
    """
    root: tkinter.Tk
    mainframe: tkinter.Frame
    _vars: dict
    _vcmd: any  # TODO what type?
    _logic: LearnerLogic
    _shuffled_questions: List[dict]
    _total_questions_no: int
    _incorrect_answers: List[int]
    _default_x_padding = 5,
    _default_y_padding = 5,
    _feedback: bool
    _feedback_str: str

    def __init__(self, root: tkinter.Tk, logic: LearnerLogic):
        self.root = root
        self.root.geometry("500x500")
        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)
        self._init_frame()
        self._logic = logic
        self._interrupt = False
        self._vars = {
            'lessons_files': tkinter.StringVar(value=self._logic.get_files_list())
        }
        self._vcmd = (root.register(self.pos_int_tkinter_validate),
                      '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')

    # noinspection PyUnusedLocal
    @staticmethod
    def pos_int_tkinter_validate(action, index, value_if_allowed,
                                 prior_value, text, validation_type, trigger_type, widget_name) -> bool:
        """
        Validate only digits input. Args required by tkinter
        """
        if text in '0123456789':
            try:
                int(value_if_allowed)
                return True
            except ValueError:
                return False
        return False

    def _init_frame(self) -> None:
        """
        Initialize main frame that is base for other widgets
        """
        self.mainframe = ttk.Frame(self.root, padding="10 10 10 10")
        self.mainframe.grid(sticky=(N, S, E, W))

    def _clear_frame(self) -> None:
        """
        Destroy everything on frame before drawing new window
        """
        for child in self.mainframe.winfo_children():
            child.destroy()
        self.mainframe.destroy()
        self._init_frame()

    def _set_grid(self, rows_weights: List[int], cols_weights: List[int]) -> None:
        """
        Set grid parameters for the view.
        :param rows_weights: Weights for rows in grid.
        :param cols_weights: Weight for columns in grid.
        """
        for col_index in range(0, len(cols_weights)):
            self.mainframe.columnconfigure(col_index, weight=cols_weights[col_index])
        for row_index in range(0, len(rows_weights)):
            self.mainframe.rowconfigure(row_index, weight=rows_weights[row_index])

    def start_correction_quiz(self) -> None:
        """
        Initialize variables for quiz where user only corrects their incorrect answers.
        """
        self._total_questions_no = len(self._incorrect_answers)
        self._shuffled_questions = self._logic.get_correction_questions(self._shuffled_questions,
                                                                        self._incorrect_answers)
        self._incorrect_answers = []
        self._feedback = False
        self._feedback_str = ''
        self._draw_single_quiz(0)

    def start_quiz(self, questions: [dict], total_questions_no: int) -> None:
        """
        Initialize variables for new quiz.
        :param questions: List of all questions.
        :param total_questions_no: Number of questions that should be in quiz.
        """
        self._total_questions_no = total_questions_no
        self._shuffled_questions = list(questions)
        random.shuffle(self._shuffled_questions)
        self._incorrect_answers = []
        self._feedback = False
        self._feedback_str = ''
        self._draw_single_quiz(0)

    def _create_button(self, text: str, column: int, row: int, command: Callable[[], None]) -> ttk.Button:
        """
        Create button in the grid.
        :param text: Text on button.
        :param column: Column in the grid.
        :param row: Row in the grid.
        :param command: Command for the button.
        :return: Created button.
        """
        button = ttk.Button(self.mainframe, text=text, command=command)
        button.grid(column=column, row=row, sticky=NSEW, pady=self._default_y_padding, padx=self._default_x_padding)
        return button

    def _create_label(self, text: str, column: int, row: int, column_span: Optional[int] = None,
                      sticky: any = NSEW) -> ttk.Label:
        """
        Create label in the grid
        :param text: Text on label.
        :param column: Column in the grid.
        :param row: Row in the grid.
        :param sticky: Stickiness in the grid.
        :return: Created label.
        """
        label = ttk.Label(self.mainframe, text=text, anchor=tkinter.CENTER)
        if column_span is None:
            label.grid(column=column, row=row, sticky=sticky)
        else:
            label.grid(column=column, row=row, columnspan=column_span, sticky=sticky)
        return label

    def _create_listbox(self, list_variable_name: str, box_variable_name: str, column: int, row: int,
                        sticky: any= (N, W)) -> None:
        """
        Create listbox. Their variables are written to dictionary because, from past experience, otherwise there may be
            a problem with CG removing their tkinter text variables. TODO check if this is still necessary.
        :param list_variable_name: Name for variable used for list of values.
        :param box_variable_name: Name for variable used
        :param column: Column in the grid.
        :param row: Row in the grid.
        :param sticky: Stickiness in the grid.
        :return: Nothing. Listbox can be referenced from variables.
        """
        self._vars[box_variable_name] = tkinter.Listbox(self.mainframe, listvariable=self._vars[list_variable_name],
                                                        height=5, selectmode='single')
        self._vars[box_variable_name].grid(column=column, row=row, sticky=sticky)

    def _end_quiz(self) -> None:
        """
        Ending the quiz.
        If all answers were correct just go to lesson menu.
        Otherwise give user option to repeat just the questions that they gave incorrect answers to.
        """
        if len(self._incorrect_answers) == 0:
            self._draw_lesson_menu(None)
            return

        self._clear_frame()
        self._set_grid([1, 8, 1], [1, 1])

        ttk.Label(self.mainframe,
                  text=f'Final result: {self._total_questions_no - len(self._incorrect_answers)}'
                       f'/{self._total_questions_no}.\nDo you want to repeat incorrectly answered questions?',
                  anchor=tkinter.CENTER).grid(column=0, row=0,
                                              columnspan=2,
                                              sticky=NSEW)

        self._create_button('Yes', 0, 4, lambda: self.start_correction_quiz())
        self._create_button('No', 1, 4, lambda: self._draw_lesson_menu(None))

    def _evaluate_answer(self, current_index: int, question: dict, answer: str, callback: Callable[[], None]) -> None:
        """
        Evaluate given answer and continue quiz.
        :param current_index: Index of the evaluated question.
        :param question: Evaluated question data.
        :param answer: Answer given by user.
        :param callback: Callback for function to be executed after evaulation.
        """
        (self._feedback, self._feedback_str) = self._logic.evaluate_answer(question, answer)
        if not self._feedback:
            self._incorrect_answers.append(current_index)
        callback()

    def _draw_single_quiz(self, question_no: int) -> None:
        """
        Draw single question in quiz. User can either make answer or exit the quiz.
        :param question_no:  Number of current question.
        """
        self._clear_frame()
        self._set_grid([2, 2, 5, 5, 1], [1, 1])

        current_question = self._shuffled_questions[question_no]

        self._create_label(f'Question {question_no + 1}/{self._total_questions_no}', 0, 0, 2)
        feedback = self._create_label(self._feedback_str, 0, 1, 2)
        if self._feedback:
            feedback.configure(foreground="green")
        else:
            feedback.configure(foreground="red")

        self._create_label(current_question['question'], 0, 2, 2)

        answer = tkinter.StringVar()
        answer_entry = ttk.Entry(self.mainframe, textvariable=answer)
        answer_entry.grid(column=0, row=3, columnspan=2, sticky=NSEW)
        answer_entry.focus()

        select_button_text = "Next"
        if question_no == self._total_questions_no - 1:
            select_button_text = "Finish"

        def next_button():
            self.root.unbind('<Return>')
            if question_no == self._total_questions_no - 1:
                self._end_quiz()
            else:
                self._draw_single_quiz(question_no + 1)

        select_button = self._create_button(select_button_text, 0, 4,
                                            lambda: self._evaluate_answer(question_no, current_question, answer.get(),
                                                                          next_button))
        select_button['state'] = tkinter.DISABLED
        self._create_button('Exit', 1, 4, lambda: self._draw_lesson_menu(None))

        def answer_typed(_1, _2, _3):
            if answer.get():
                select_button['state'] = tkinter.NORMAL

        answer.trace_add('write', answer_typed)

        def try_next_button(_):
            if answer.get():
                self._evaluate_answer(question_no, current_question, answer.get(), next_button)

        self.root.bind('<Return>', try_next_button)

    def _draw_number_menu(self, new_selected_topic: Optional[str]) -> None:
        """
        Draw menu where user selects how much exercises from selected topic they want to do or go back to lesson menu.
        :param new_selected_topic: If user arrived to this menu from lesson menu then selected topic need to be updated.
        """
        if not (new_selected_topic is None):
            self._vars['selected_topic'] = new_selected_topic

        self._clear_frame()
        self._set_grid([2, 2, 1, 8, 1], [1, 1])
        selected_lesson = self._vars['selected_lesson']
        selected_lesson_topic = self._vars['selected_topic']
        questions = self._logic.get_questions(selected_lesson, selected_lesson_topic)
        self._vars['selected_questions'] = questions
        self._create_label(f'Select lesson: {selected_lesson} \nSelected lesson topic: {selected_lesson_topic}', 0, 0,
                           2)

        error_label = self._create_label('', 0, 1, 2)
        error_label.configure(foreground="red")

        self._create_label(f'Total questions no: {len(questions)}', 0, 2, 2)

        def set_error(error_text: str):
            error_label['text'] = error_text

        self._create_label('Questions in quiz: ', 0, 3)
        self._vars['selected_no'] = tkinter.IntVar()
        self._vars['selected_no'].set(len(questions))
        exercise_no_entry = ttk.Entry(self.mainframe, validate='key', validatecommand=self._vcmd,
                                      textvariable=self._vars['selected_no'])
        exercise_no_entry.grid(column=1, row=3, sticky=EW)

        self._create_button('Start', 0, 4,
                            lambda: Utils.validate(
                                validate_function=lambda: Utils.is_int(self._vars['selected_no'].get()) and
                                                          0 < self._vars['selected_no'].get() <= len(questions),
                                positive_callback=lambda: self.start_quiz(questions, self._vars['selected_no'].get()),
                                negative_callback=lambda: set_error(f'Enter number between 1 and {len(questions)}')
                            ))
        self._create_button('Back', 1, 4, lambda: self._draw_lesson_menu(None))

    def _draw_lesson_menu(self, new_selected_lesson: Optional[str]) -> None:
        """
        Draw lesson menu where user can select topic from lesson, or go back to main menu.
        :param new_selected_lesson: If not None then user arrived here from main menu and selected lesson need to be
            updated.
        """
        if not (new_selected_lesson is None):
            self._vars['selected_lesson'] = new_selected_lesson

        self._clear_frame()
        self._set_grid([2, 8, 1], [1, 1])
        selected_lesson = self._vars['selected_lesson']

        self._create_label(f'Select lesson: {selected_lesson}', 0, 0, 2)
        self._create_label('Select Topic:', 0, 1, sticky=(N, W))
        self._vars['topics'] = tkinter.StringVar(value=self._logic.get_topics_list(selected_lesson))
        self._create_listbox('topics', 'topics_box', 1, 1)
        select_button = self._create_button('Select Topic', 0, 2, lambda: self._draw_number_menu(
                                       self._logic.get_topics_list(selected_lesson)[
                                           self._vars['topics_box'].curselection()[0]]
                                   ))
        select_button['state'] = tkinter.DISABLED
        self._create_button('Back', 1, 2, lambda: self._draw_main_menu())

        def topic_selected(_):
            select_button['state'] = tkinter.NORMAL

        self._vars['topics_box'].bind('<<ListboxSelect>>', topic_selected)

    def _draw_main_menu(self) -> None:
        """
        Draw the main menu where user can either select lesson or exit the program.
        """
        self._clear_frame()
        self._set_grid([2, 8, 1], [1, 1])

        self._create_label('Dutch Learner', 0, 0, 2)
        self._create_label('Select exercise file:', 0, 1, sticky=(N, W))
        self._create_listbox('lessons_files', 'lessons_files_box', 1, 1)
        select_button = self._create_button('Select Lesson', 0, 2, lambda: self._draw_lesson_menu(
                                       self._logic.get_files_list()[self._vars['lessons_files_box'].curselection()[0]]))
        select_button['state'] = tkinter.DISABLED
        self._create_button('Exit', 1, 2, lambda: self.root.destroy())

        def lesson_selected(_):
            select_button['state'] = tkinter.NORMAL

        self._vars['lessons_files_box'].bind('<<ListboxSelect>>', lesson_selected)

    def start(self) -> None:
        self._draw_main_menu()