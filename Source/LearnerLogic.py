from typing import Optional, List
import random
import json
import Utils
import os
from Utils import LessonNotFoundException, TopicNotFoundException


class LearnerLogic():
    def get_files_list(self) -> [str]:
        """
        Give list of available lesson files.
        :return: List of available lessons files. Without file extension and path.
        """
        list_dirs = Utils.get_file_dirs(Utils.base_path())
        path = os.listdir(list_dirs['test'])
        if len(path) == 0 or (not Utils.is_debug_env()):
            path = os.listdir(list_dirs['local'])
        if len(path) == 0:
            path = os.listdir(list_dirs['default'])
        json_names = list(map(lambda y: y[0:-5], filter(lambda x: x[-5::] == '.json', path)))
        return json_names

    @staticmethod
    def _get_file_path(filename: str, base_path: str) -> dict:
        """
        Get file paths for test, local and default environment.
        :param filename: Name of the file.
        :param base_path: Path of the program root.
        :return: Dictionary with 3 possible paths.
        """
        return {
            'test': os.path.join(os.path.join(base_path, os.path.join('..', 'TestData')), filename + '.json'),
            'default': os.path.join(os.path.join(base_path, os.path.join('..', 'Data')), filename + '.json'),
            'local': os.path.join(os.path.join(base_path, os.path.join('..', 'LocalData')), filename + '.json'),
        }

    @staticmethod
    def _try_get_file(filename: str, base_path: str) -> Optional[dict]:
        """
        Try to get JSON file.
        First try to find it in test path.
        If it is not there try to find it in local path.
        Otherwise try to find it in default path.
        :param filename: Name of the file.
        :param base_path: Path of the program root.
        :return: Dictionary of parsed JSON file or None if file was not found.
        """
        paths = LearnerLogic._get_file_path(filename, base_path)
        path = None
        if Utils.is_debug_env() and os.path.isfile(paths['test']):
            path = paths['test']
        elif os.path.isfile(paths['local']):
            path = paths['local']
        elif os.path.isfile(paths['default']):
            path = paths['default']
        if not path:
            return None
        try:
            with open(path) as data_file:
                return json.load(data_file)
        except json.decoder.JSONDecodeError as e:
            print("Error in parsing JSON file", e)
            return None

    def get_topics_list(self, lesson_name: str) -> [str]:
        """
        Get list of topics for particular lesson.
        :param lesson_name: Name of the file with lesson.
        :return: List of topics.
        """
        data = LearnerLogic._try_get_file(lesson_name, Utils.base_path())
        if data is None:
            raise LessonNotFoundException()
        else:
            return list(data.keys())

    def get_questions(self, lesson_name: str, topic_name: str) -> List[dict]:
        """
        Get question for given lesson topic.
        :param lesson_name: Name of the lesson.
        :param topic_name: Name of the topic.
        :return: List of questions.
        """
        data = LearnerLogic._try_get_file(lesson_name, Utils.base_path())
        if data is None:
            raise LessonNotFoundException()
        else:
            if topic_name not in data:
                raise TopicNotFoundException()
            return data[topic_name]

    def evaluate_answer(self, question_dict: dict, answer: str) -> (bool, str):
        """
        Evaluate answer given by user.
        :param question_dict: Question data.
        :param answer: Answer given by user.
        :return: Pair that contains:
            bool that is true if answer was correct.
            string that is text feedback for user.
        """
        good_answer_feedback = 'Correct'
        bad_answer_feedback = f'Answer {answer} is incorrect for question {question_dict["question"]}.' \
                              f' Correct answer: {question_dict["answer"]}'
        if answer == question_dict['answer']:
            return True, good_answer_feedback
        if not ('other_answers' in question_dict):
            return False, bad_answer_feedback
        if answer in question_dict['other_answers']:
            return True, good_answer_feedback
        return False, bad_answer_feedback

    def get_correction_questions(self, questions: [dict], incorrect_answers: [int]) -> [dict]:
        """
        Get questions for user who want to try again questions they answered incorrectly.
        :param questions: List of questions that were asked to user.
        :param incorrect_answers: List of indexes of questions that user answered incorrectly.
        :return: List of new questions for user to answer.
        """
        new_questions = []
        for index in incorrect_answers:
            new_questions.append(questions[index])
        random.shuffle(new_questions)
        return new_questions
